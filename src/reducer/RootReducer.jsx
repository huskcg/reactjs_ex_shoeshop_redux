import { combineReducers } from "redux";
import { ShoeReducer } from "./ShoeReducer";

export const RootReducer = combineReducers({
  ShoeReducer: ShoeReducer,
});
