import { ADD_TO_CART, CHANGE_QUANTITY, DELETE } from "../constant/ShoeContants";
import { data } from "../data/data";

let initialValue = {
  data: data,
  cart: [],
};

export const ShoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE: {
      let index = state.cart.findIndex((item) => {
        return item.id === action.payload;
      });
      if (index !== -1) {
        let newCart = [...state.cart];
        newCart.splice(index, 1);
        return { ...state, cart: newCart };
      }
      break;
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      cloneCart[index].soLuong += action.payload.soLuong;
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
