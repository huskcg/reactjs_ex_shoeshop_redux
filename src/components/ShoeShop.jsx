import React, { Component } from "react";
import Cart from "./Cart";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  render() {
    return (
      <div className="container">
        <h2 style={{ textAlign: "center" }}>ShoeShop</h2>
        <Cart />
        <ListShoe />
      </div>
    );
  }
}
